<?php
require ".htinclude.php";
if(isset($_POST['id'], $_POST['pw'], $_POST['pc']) && $_POST['pw'] == $_POST['pc'])
{
    $stmt = $conn->prepare("INSERT IGNORE INTO `{$prefix}_users` (`id`, `salt`, `pw`) VALUES (?, ?, ?)");
    if($stmt === false)
    {
        echo '<meta charset="UTF-8" /><script>alert("오류 발생");history.back();</script>';
        $conn->close();
        exit();
    }
    $salt = bin2hex(random_bytes(16));
    $pass = hash_pbkdf2("sha256", $_POST['pw'], $salt, 1000, 64, false);
    $stmt->bind_param("sss", $_POST['id'], $salt, $pass);
    $stmt->execute();
    if($stmt->affected_rows !== 1)
    {
        echo '<meta charset="UTF-8" /><script>alert("가입 실패");history.back();</script>';
        $stmt->close();
        $conn->close();
        exit();
    }
    $id = $stmt->insert_id;
    $stmt->close();
    $stmt = $conn->prepare("INSERT INTO {$prefix}_pw_history (usr, pw) VALUES (?, ?)");
    if($stmt === false)
    {
        echo "An error has occurred<br />\n" . $conn->errno . " : " . $conn->error;
        $conn->close();
        exit();
    }
    $stmt->bind_param("ss", $id, $pass);
    $stmt->execute();
    $stmt->close();
    $conn->close();
    echo '<meta charset="UTF-8" /><script>alert("가입 성공");location.href=".";</script>';
}
else
{
    echo '<meta charset="UTF-8" /><script>alert("비밀번호 다름");history.back();</script>';
}
$conn->close();
?>