<?php
if(@include(".ht"))
{
    unlink(__FILE__);
    header("Location: .");
    $conn->close();
    exit();
}
if(isset($_POST['dbhost'], $_POST['dbname'], $_POST['dbuser'], $_POST['dbpass'], $_POST['prefix'], $_POST['id'], $_POST['pw']))
{
    $conn = @mysqli_connect($_POST['dbhost'], $_POST['dbuser'], $_POST['dbpass'], $_POST['dbname']) or die("Failed to connect");
    if(!$conn) die("Failed to connect<br />\n" . mysqli_conn_errno() . " : " . mysqli_conn_error());
    if(!$conn->query("CREATE TABLE $_POST[prefix]_users(
        seq INT PRIMARY KEY AUTO_INCREMENT,
        id VARCHAR(20) NOT NULL,
        salt BINARY(32) NOT NULL,
        pw BINARY(64) NOT NULL,
        UNIQUE INDEX(id))"))
    {
        echo "Failed to create users table<br />\n" . $conn->errno . " : " . $conn->error;
        $conn->close();
        exit();
    }
    if(!$conn->query("CREATE TABLE $_POST[prefix]_pw_history(
        usr INT NOT NULL,
        pw BINARY(64) NOT NULL,
        PRIMARY KEY(usr, pw),
        FOREIGN KEY(usr) REFERENCES $_POST[prefix]_users(seq))"))
    {
        echo "Failed to create password history table<br />\n" . $conn->errno . " : " . $conn->error;
        $conn->close();
        exit();
    }
    $stmt = $conn->prepare("INSERT INTO $_POST[prefix]_users (id, salt, pw) VALUES (?, ?, ?)");
    if($stmt === false)
    {
        echo "An error has occurred<br />\n" . $conn->errno . " : " . $conn->error;
        $conn->close();
        exit();
    }
    $salt = bin2hex(random_bytes(16));
    $pass = hash_pbkdf2("sha256", $_POST['pw'], $salt, 1000, 64, false);
    $stmt->bind_param("sss", $_POST['id'], $salt, $pass);
    $stmt->execute();
    if($stmt->affected_rows !== 1)
    {
        echo "Failed to create admin user<br />\n" . $conn->errno . " : " . $conn->error;
        $conn->close();
        exit();
    }
    $id = $stmt->insert_id;
    $stmt->close();
    $stmt = $conn->prepare("INSERT INTO $_POST[prefix]_pw_history (usr, pw) VALUES (?, ?)");
    if($stmt === false)
    {
        echo "An error has occurred<br />\n" . $conn->errno . " : " . $conn->error;
        $conn->close();
        exit();
    }
    $stmt->bind_param("ss", $id, $pass);
    $stmt->execute();
    if($stmt->affected_rows !== 1)
    {
        echo "Failed record admin password history<br />\n" . $conn->errno . " : " . $conn->error;
        $conn->close();
        exit();
    }
    $stmt->close();
    $conn->close();
    if(file_put_contents(".ht", '<?php
$conn = new mysqli("' . addslashes($_POST['dbhost']) . '", "' . addslashes($_POST['dbuser']) . '", "'
    . addslashes($_POST['dbpass']) . '", "' . addslashes($_POST['dbname']) . '");
$prefix = "' . addslashes($_POST['prefix']) . '";
?>') === false)
    {
        echo "Failed to write setting file";
        exit();
    }
    unlink(__FILE__);
    echo '<script>alert("DB 설정 성공");location.href=".";</script>';
    exit();
}
?><!DOCTYPE HTML>
<html>
    <head>
        <meta charset="UTF-8" />
        <title>데이터베이스 설정</title>
    </head>
    <body>
        <form method="POST">
            <table>
                <tr>
                    <th>DB 호스트</th>
                    <td><input name="dbhost" type="text" />
                </tr>
                <tr>
                    <th>DB 이름</th>
                    <td><input name="dbname" type="text" />
                </tr>
                <tr>
                    <th>DB 사용자</th>
                    <td><input name="dbuser" type="text" />
                </tr>
                <tr>
                    <th>DB 비밀번호</th>
                    <td><input name="dbpass" type="text" />
                </tr>
                <tr>
                    <th>DB 테이블 접두사</th>
                    <td><input name="prefix" type="text" />
                </tr>
                <tr>
                    <th>Admin 사용자</th>
                    <td><input name="id" type="text" />
                </tr>
                <tr>
                    <th>Admin 비밀번호</th>
                    <td><input name="pw" type="text" />
                </tr>
                <tr>
                    <td><button type="submit">확인</button></td>
                </tr>
            </table>
        </form>
    </body>
</html>
