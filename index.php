<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <title>로그인</title>
    </head>
    <body><?php
session_start();
if(!isset($_SESSION['seq']))
{
    ?>
        <h1>You are not logged in.</h1>
        <h2><a href="login.html">Login</a></h2><?php
}
else
{
    ?>
        <h1>Hello <?=htmlspecialchars($_SESSION['username'])?></h1>
        <h2><a href="logout.php">Logout</a></h2><?php
}
?>
    </body>
</html>