# Login

기본적인 로그인 / 회원가입 기능



## License

누구든 어떤 용도로도 자유롭게 수정/배포/사용 가능합니다.

직·간접적인 어떤 종류의 모든 보증을 부인합니다.



## Getting Started

서버 설정하는 방법



### Prerequisites

웹 서버에서 PHP를 사용할 수 있어야 합니다.

회원정보를 저장할 MySQL 데이터베이스도 필요합니다.



### Setting

처음 로그인이나 회원가입을 시도할 때 DB 정보를 설정하는 페이지가 나옵니다.

※ 주의 : DB를 설정하는 부분에는 **SQL 인젝션 방지가 적용되어 있지 않**습니다.

**DB 정보를 다른 사람이 설정하지 못하도록** 반드시 로컬에서 설정하시기 바랍니다.

※ 주의 : 비밀번호를 포함한 DB 정보를 **암호화 없이** `.ht` 파일에 저장합니다.

설정 중 오류가 발생하면 **생성된 테이블을 직접 삭제**한 후 다시 설정해야 합니다.



## Deployment

DB를 설정하지 않았으면 위 Setting의 내용대로 하면 됩니다.

**이미 설정한 DB를 사용**하려면 DB를 설정한 서버의 `.ht`파일을 복사해야 합니다.



## Contributing

간단한 수정이라도 PR 환영합니다.

```C
if(cond)
{
    something();
}
```

줄바꿈 많이 쓰고, 인덴트는 Space 네 개로 해 주세요.



## Authors

- 맹주영 - _Initial work_ - [mjy9088](https://gitlab.com/mjy9088)



## Acknowledgment

- 보안 문제 지적은 언제나 환영합니다!